#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>

#define MAX_LINE 1000000
#define LINSTENPORT 7788
#define SERVERPORT 8000
#define BUFFSIZE 1000000


void writefile(int sockfd, FILE *fp, long long int size)
{
    ssize_t n;
    char buff[MAX_LINE] = {0};
    printf("Processing.... 0%");
    while ((n = recv(sockfd, buff, MAX_LINE, 0)) > 0) 
    {
	    total+=n;
        if (n == -1)
        {
            perror("\nReceive File Error");
            exit(1);
        }
        
        if (fwrite(buff, sizeof(char), n, fp) != n)
        {
            perror("\nWrite File Error");
            exit(1);
        }
        memset(buff, 0, MAX_LINE);
        long long int cal = (total *100)/size;
        printf("\rProcessing.... %lld%%", cal);
    }
}
ssize_t total=0;
int main(int argc, char* argv[])
{
    if (argc < 2) 
    {
        printf("No file names given\n");
        exit(1);
    }

    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
    {
        perror("\nCan't allocate sockfd");
        exit(1);
    }

    struct sockaddr_in serveraddr;
    memset(&serveraddr, 0, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_port = htons(SERVERPORT);
    if (inet_pton(AF_INET, "127.0.0.1", &serveraddr.sin_addr) < 0)
    {
        perror("\nIPaddress Convert Error");
        exit(1);
    }

    if (connect(sockfd, (const struct sockaddr *) &serveraddr, sizeof(serveraddr)) < 0)
    {
        perror("\nConnect Error");
        exit(1);
    }
    
    char *filename = basename(argv[1]); 
    if (filename == NULL)
    {
        perror("\nCan't get filename");
        exit(1);
    }
    
    char buff[BUFFSIZE] = {0};
    strncpy(buff, filename, strlen(filename));
    if (send(sockfd, buff, BUFFSIZE, 0) == -1)
    {
        perror("\nCan't send filename");
        exit(1);
    }


    FILE *fp = fopen(argv[1], "wb");
    if (fp == NULL) 
    {
        perror("\nCan't open file");
        exit(1);
    }

    long long int num;

    recv(sockfd, &num, sizeof(num), 0);

    writefile(sockfd,fp,num);
    if(total==0)
    {
        printf("File:%s not Found on the server side\n",filename);
        remove(filename);
        exit(0);
    }
    printf("\rProcessing.... 100%\n");
    printf("\nRecieve Success\n");
    fclose(fp);
    close(sockfd);
    return 0;
}


