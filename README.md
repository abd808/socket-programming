# Socket Programming in C
- File transmission over TCP sockets in Linux system.

- Creates a Client - Server system to act as communication channel for downloading files

- Handles transmission of large files (>1GB) as well.


## Compile

`make`

(in main directory)

    
## Usage

run :

`./get.sh filename1 filename2 ...   filename20 ....`  


- The server and client executables should be put into their respective directories ..    
    
- Files should be present in the "Server" directory or its subdirectories, and name of file should be given assuming home directory as server directory.

- The files will be downloaded in the "Client" directory

- There is no limit on the number of files that can be requested in each command

