#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>

#define MAX_LINE 1000000
#define LINSTENPORT 7788
#define SERVERPORT 8000
#define BUFFSIZE 1000000


void sendfile(FILE *fp, int sockfd, long long int size) 
{
    long long int n; 
    char sendline[MAX_LINE] = {0}; 
    while ((n = fread(sendline, sizeof(char), MAX_LINE, fp)) > 0) 
    {
	    total+=n;
        if (n != MAX_LINE && ferror(fp))
        {
            perror("\nRead File Error");
            exit(1);
        }
        
        if (send(sockfd, sendline, n, 0) == -1)
        {
            perror("\nCan't send file");
            exit(1);
        }
        memset(sendline, 0, MAX_LINE);
    }
}
ssize_t total=0;
int main(int argc, char *argv[]) 
{
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1) 
    {
        perror("\nCan't allocate sockfd");
        exit(1);
    }
    
    struct sockaddr_in clientaddr, serveraddr;
    memset(&serveraddr, 0, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
    serveraddr.sin_port = htons(SERVERPORT);

    if (bind(sockfd, (const struct sockaddr *) &serveraddr, sizeof(serveraddr)) == -1) 
    {
        perror("\nBind Error");
        exit(1);
    }

    if (listen(sockfd, LINSTENPORT) == -1) 
    {
        perror("\nListen Error");
        exit(1);
    }

    socklen_t addrlen = sizeof(clientaddr);
    int connfd = accept(sockfd, (struct sockaddr *) &clientaddr, &addrlen);
    if (connfd == -1) 
    {
        perror("\nConnect Error");
        exit(1);
    }
    close(sockfd); 

    char filename[BUFFSIZE] = {0}; 
    if (recv(connfd, filename, BUFFSIZE, 0) == -1) 
    {
        perror("\nCan't receive filename");
        exit(1);
    }

    long long int status_or_size;//-1 if file doesnt exist, else size of file
    FILE *fp = fopen(filename, "rb");
    if (fp == NULL) 
    {   
        status_or_size=-1;
        perror("\nCan't open file");
        exit(1);
    }
    else
    {
        struct stat st;
        stat(filename, &st);
        status_or_size = st.st_size;
    }

    if (send(connfd, &status_or_size, sizeof(status_or_size), 0) == -1)
    {
        perror("\nCan't send file\n");
        exit(1);
    }

    sendfile(fp,connfd,status_or_size);
     printf("\nSend Success, NumBytes = %ld\n", total);
    fclose(fp);
    close(connfd);
    return 0;
}



