all : client server
.PHONY : all
client : client.c 
	gcc -Wall -O2 client.c -o client
server : server.c 
	gcc -Wall -O2 server.c -o server
clean :
	rm client server
