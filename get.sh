#!/bin/bash

for var in "$@"
do
    (cd ./Server && exec ./server) &
    (cd ./Client && ./client "$var")
done